#ifndef __HEX_IO_H__
#define __HEX_IO_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <hex_base.h>
#include <hex_error.h>
#include <hex_check.h>
#include <hex_print.h>

#define HEX_NUM_LINES 1024
#define HEX_SYMB_IN_LINE 512
#define HEX_PRINT_WIDTH 16

typedef struct _hexFile_t
{
    const char *path;
    FILE *fd;
} hexFile_t;

// Depending on the mode open existing file on filesystem or
// create and open new file
hexError_t
hex_open_file(hexFile_t **file, const char *path, const char *mode);

// Close existing file on filesystem, but don't delete them
hexError_t
hex_close_file(hexFile_t *file);

// Close and delete existing file on filesystem
hexError_t
hex_delete_file(hexFile_t *file);

// Load and parse file into memory
hexError_t
hex_read_file_content(hex_line ***hexl, size_t *elem, hexFile_t *file);

hexError_t
hex_fmt_write_hex_byte_to_file(uint8_t hex, hexFile_t *file,
                               size_t start_pos, size_t max);

hexError_t
hex_write_hex_byte_to_file(uint8_t hex, hexFile_t *file, const char *delim);

// Writes data to the current position of file
hexError_t
hex_fmt_write_hex_line_data(hex_line *hexl, hexFile_t *file,
                            size_t start_pos, size_t max);

hexError_t
hex_calculate_delimeter(const char **out, size_t current, size_t max);

hexError_t
hex_write_start_header_to_file(const char *filename, hexFile_t *file);

hexError_t
hex_write_end_header_to_file(hexFile_t *file);

// Writes array of data to the current position of file
hexError_t
hex_write_array_to_file(hex_line **hexl, size_t elem,
                        size_t max, hexFile_t *file);

#endif
