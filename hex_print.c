#include <inttypes.h>

#include <hex.h>

void print_help(const char *command)
{
    if (command)
    {
        printf("%s: <file.hex> <size> or \n", command);
        printf("%s: --help or -h to print this help message\n", command);
        printf("%s: converts Intel HEX file to the C header\n", command);
        printf("<file.hex> is the regular file in Intel HEX format\n");
        printf("<size> is the size of the array in bytes\n");
    }
}

void print_simple_help(const char *command)
{
    if(command)
    {
        printf("%s: <file.hex> <size>\n", command);
    }
}

void print_hex_line_to_err(hex_line *hexl)
{
    if (!hexl)
    {
        HEX_DEBUG("Incorrect input data: 0x%"PRIXPTR"\n", (uintptr_t) hexl);
    }

    fprintf(stderr, "hex line:"
            " .count = 0x%"PRIX8
            ", .address = 0x%"PRIX16
            ", .record = 0x%"PRIX8,
            hexl->count, hexl->address,
            hexl->record);

    fprintf(stderr, ", .data = ");
    for (int k = 0; k < hexl->count; ++k)
    {
        fprintf(stderr, "[0x%"PRIX8"]",
                (hexl->data)[k]);
    }
    //fprintf(stderr, "\n");
    fprintf(stderr, ", .checksum = 0x%"PRIX8"\n",
            hexl->checksum);
}