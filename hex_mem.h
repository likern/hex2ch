#ifndef __HEX_MEM_H__
#define __HEX_MEM_H__

#include <hex_error.h>

// Allocating memory function.
hexError_t
hex_malloc(void **buffer, size_t size);

// Free previously allocated memory
void
hex_free(void *ptr);

#endif