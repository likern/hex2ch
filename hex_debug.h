#ifndef __HEX_DEBUG_H__
#define __HEX_DEBUG_H__

#define HEX_DEBUG(fmt, arg...) \
    do { \
        fprintf(stderr, "%s:%s(%d)> "fmt"\n", __FILE__,\
                __func__, __LINE__, ##arg); \
    } while (0)

#endif