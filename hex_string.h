#ifndef __HEX_STRING_H__
#define __HEX_STRING_H__

#include <hex_error.h>

hexError_t
hex_str_to_upper(char **string, const char *filename);

#endif
