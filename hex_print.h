#ifndef __HEX_PRINT_H__
#define __HEX_PRINT_H__

// Print help message how to use program
void print_help(const char *command);

// Print simplified help message
void print_simple_help(const char *command);
void print_hex_line_to_err(hex_line *hexl);
#endif
