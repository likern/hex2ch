#ifndef __HEX_ERROR_H__
#define __HEX_ERROR_H__

// This header contains all possible exit codes
// of application and high-level functions

// We guarantee that overall number of errors
// do not exceed 8 bytes and can be used as 
// exit() error code

#include <hex_debug.h>

typedef char hexError_t; 

// Indicate successfull completion, without errors
#define HEX_ESUCCESS 0
// Indicate general error
#define HEX_EFAILURE 1
// Indicate system call error
#define HEX_ESYSTEM 2
// Invalid arguments
#define HEX_EINVARG 3
// Conversion failed
#define HEX_EOUTRANGE 4
// Conversion failed
#define HEX_ECONVERSION 5
// Memory allocation problem
#define HEX_EALLOCATION 6
// Invalid checksum of data
#define HEX_ECHECKSUM 7
// Invalid type of record field
#define HEX_ERECORD 8
// I/O operation error
#define HEX_EIO 9
// No some permissions
#define HEX_EPERM 10
#define HEX_EOVERLAP 11
#define HEX_EOUTOFBOUND 12
#define HEX_ESTRFMT 13
// Special type of error when we
// can continue working
#define HEX_ENONFATAL 14

#endif