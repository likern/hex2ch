#define _GNU_SOURCE

#include <string.h>
#include <stdio.h>
#include <inttypes.h>

#include <hex.h>

void free_hex_line(hex_line *hexl)
{
    if (hexl)
    {
        if (hexl->data)
        {
            free(hexl->data);
        }
        free(hexl);
    }
}

void free_hex_line_array(hex_line **hexl, size_t elem)
{
    if (hexl)
    {
        for (int i = 0; i < elem; ++i)
        {
            free_hex_line(hexl[i]);
        }
        free(hexl);
    }
}

hexError_t
hex_new_hex_line(hex_line **line, uint8_t count, uint16_t address,
                 uint8_t record, uint8_t *data, uint8_t checksum)
{
    if (!line)
    {
        return HEX_EINVARG;
    }

    hex_line *nline = (hex_line *) calloc(1, sizeof(hex_line));
    if (!nline)
    {
        return HEX_EALLOCATION;
    }

    nline->count = count;
    nline->address = address;
    nline->record = record;
    nline->data = data;
    nline->checksum = checksum;

    *line = nline;
    return HEX_ESUCCESS;
}

hexError_t
hex_new_hex_line_empty(hex_line **line)
{
    if (!line)
    {
        return HEX_EINVARG;
    }

    hex_line *tmp_hexl = NULL;
    hexError_t err;
    err = hex_new_hex_line(&tmp_hexl, 0x0, 0x0, 0x0, NULL, 0x0);
    if (HEX_ESUCCESS != err)
    {
        return err;
    }

    *line = tmp_hexl;
    return HEX_ESUCCESS;
}

hexError_t
hex_line_set_count(hex_line *line, uint8_t count)
{
    if (!line)
    {
        return HEX_EINVARG;
    }

    line->count = count;
    return HEX_ESUCCESS;
}

hexError_t
hex_line_set_address(hex_line *line, uint16_t address)
{
    if (!line)
    {
        return HEX_EINVARG;
    }

    line->address = address;
    return HEX_ESUCCESS;
}

hexError_t
hex_line_set_record(hex_line *line, uint8_t record)
{
    if (!line)
    {
        return HEX_EINVARG;
    }

    line->record = record;
    return HEX_ESUCCESS;
}

hexError_t
hex_line_set_data(hex_line *line, uint8_t *data)
{
    if (!line)
    {
        return HEX_EINVARG;
    }

    line->data = data;
    return HEX_ESUCCESS;
}

hexError_t
hex_line_set_checksum(hex_line *line, uint8_t checksum)
{
    if (!line)
    {
        return HEX_EINVARG;
    }

    line->checksum = checksum;
    return HEX_ESUCCESS;
}

// Sort by address field
int hex_cmp_sort(const void *first, const void *second)
{
    hex_line **fhexl = (hex_line **) first;
    hex_line **shexl = (hex_line **) second;
    return ((*fhexl)->address - (*shexl)->address);
}

hexError_t
hex_sort_hexline(void *base, size_t elem, size_t size,
                 int (*compare) (const void *, const void *))
{
    if (!base || !compare)
    {
        return HEX_EINVARG;
    }

    // FIXME: Change to the portable re-entrant sorting.
    // But as qsort_r is not supported by C99, now
    // is using qsort()
    qsort(base, elem, size, compare);
    return HEX_ESUCCESS;
}

hexError_t
hex_get_basename(char **name, const char *original)
{
    if (!name || !original)
    {
        return HEX_EINVARG;
    }

    *name = basename(original);
    return HEX_ESUCCESS;
}

hexError_t
hex_create_new_filename(char **newfname, const char *original)
{
    if (!original || !newfname)
    {
        return HEX_EINVARG;
    }

    hexError_t err;
    char *base;
    err = hex_get_basename(&base, original);
    if (HEX_ESUCCESS != err || !strcmp(base, ""))
    {
        return HEX_EINVARG;
    }

    // Create new file name to store array
    size_t ln = strlen(base);
    size_t iter = ln;

    while (iter > 0)
    {
        if (!strncmp(&base[iter], ".", 1))
        {
            break;
        }
        --iter;
    }

    if (!iter)
    {
        // Didn't find prefix. Use all string.
        iter = ln;
    }

    // 1 bytes for null character
    // 2 bytes for .c extension
    size_t length = iter;
    char *nfilename = (char *) calloc(1, length + 3);
    if (!nfilename)
    {
        return HEX_EALLOCATION;
    }

    strncpy(nfilename, base, length);
    strncpy(nfilename + length, ".c", 2);
    *newfname = nfilename;
    return HEX_ESUCCESS;
}