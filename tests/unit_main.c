#include <stdlib.h>
#include <unit_args.h>
#include <unit_line.h>
#include <unit_fname.h>
#include <check.h>

int main (void)
{
    Suite *st_input = test_input_data_suite();
    Suite *st_line = test_hex_line_suite();
    Suite *st_fname = test_filename_suite();
    
    SRunner *sr = srunner_create(st_input);
    srunner_add_suite (sr, st_line);
    srunner_add_suite (sr, st_fname);
    //srunner_add_suite(sr, st_input);
    srunner_run_all(sr, CK_NORMAL);
    int num_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (num_failed) ? EXIT_SUCCESS : EXIT_FAILURE;
}