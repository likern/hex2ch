#define _GNU_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <hex.h>
#include <unit_line.h>

static hex_line *unit_hex_line = NULL;

static void unit_emul_hexl_line_null()
{
    hex_new_hex_line(&unit_hex_line, 0x0, 0x0, 0x0, 0x0, 0x0);
}

static void unit_setup()
{
    unit_emul_hexl_line_null();
}

static void unit_teardown()
{
    free_hex_line(unit_hex_line);
}

START_TEST (test_hex_new_hex_line_null)
{   
    hexError_t err;
    err = hex_new_hex_line(NULL, 0x11, 0x12, 0x13, (uint8_t *) 0x14, 0x15);
    ck_assert(HEX_ESUCCESS != err);
}
END_TEST

START_TEST (test_hex_new_hex_line1)
{   
    hex_line *line = NULL;
    hexError_t err;
    err = hex_new_hex_line(&line, 0x11, 0x12, 0x13, (uint8_t *) 0x14, 0x15);
    ck_assert(HEX_ESUCCESS == err);
    ck_assert(0x11 == line->count);
    ck_assert(0x12 == line->address);
    ck_assert(0x13 == line->record);
    ck_assert((uint8_t *) 0x14 == line->data);
    ck_assert(0x15 == line->checksum);
    hex_line_set_data(line, NULL);
    free_hex_line(line);
}
END_TEST

START_TEST (test_hex_line_set_count)
{
    hex_line *line = NULL;
    hexError_t err;
    err = hex_new_hex_line(&line, 0x0, 0x0, 0x0, NULL, 0x0);
    ck_assert(HEX_ESUCCESS == err);
    err = hex_line_set_count(line, 0xA3);
    ck_assert(0xA3 == line->count);
    ck_assert(0x0 == line->address);
    ck_assert(0x0 == line->record);
    ck_assert(NULL == line->data);
    ck_assert(0x0 == line->checksum);
    free_hex_line(line);
}
END_TEST

START_TEST (test_hex_line_set_address)
{
    hex_line *line = NULL;
    hexError_t err;
    err = hex_new_hex_line(&line, 0x0, 0x0, 0x0, NULL, 0x0);
    ck_assert(HEX_ESUCCESS == err);
    err = hex_line_set_address(line, 0xB4);
    ck_assert(0x0 == line->count);
    ck_assert(0xB4 == line->address);
    ck_assert(0x0 == line->record);
    ck_assert(NULL == line->data);
    ck_assert(0x0 == line->checksum);
    free_hex_line(line);
}
END_TEST

START_TEST (test_hex_line_set_record)
{
    hex_line *line = NULL;
    hexError_t err;
    err = hex_new_hex_line(&line, 0x0, 0x0, 0x0, NULL, 0x0);
    ck_assert(HEX_ESUCCESS == err);
    err = hex_line_set_record(line, 0xC5);
    ck_assert(0x0 == line->count);
    ck_assert(0x0 == line->address);
    ck_assert(0xC5 == line->record);
    ck_assert(NULL == line->data);
    ck_assert(0x0 == line->checksum);
    free_hex_line(line);
}
END_TEST

START_TEST (test_hex_line_set_data)
{
    hex_line *line = NULL;
    hexError_t err;
    err = hex_new_hex_line(&line, 0x0, 0x0, 0x0, NULL, 0x0);
    ck_assert(HEX_ESUCCESS == err);
    err = hex_line_set_data(line, (uint8_t *) 0xD6);
    ck_assert(0x0 == line->count);
    ck_assert(0x0 == line->address);
    ck_assert(0x0 == line->record);
    ck_assert((uint8_t *) 0xD6 == line->data);
    ck_assert(0x0 == line->checksum);
    hex_line_set_data(line, NULL);
    free_hex_line(line);
}
END_TEST

START_TEST (test_hex_line_set_checksum)
{
    hex_line *line = NULL;
    hexError_t err;
    err = hex_new_hex_line(&line, 0x0, 0x0, 0x0, NULL, 0x0);
    ck_assert(HEX_ESUCCESS == err);
    err = hex_line_set_checksum(line, 0xE7);
    ck_assert(0x0 == line->count);
    ck_assert(0x0 == line->address);
    ck_assert(0x0 == line->record);
    ck_assert(NULL == line->data);
    ck_assert(0xE7 == line->checksum);
    free_hex_line(line);
}
END_TEST

// Testing hex_check_checksum() function
START_TEST (test_hex_check_checksum1)
{
    hexError_t err;
    err = hex_check_checksum(NULL);
    ck_assert(HEX_ESUCCESS != err);
}
END_TEST

START_TEST (test_hex_check_checksum2)
{
    hex_line_set_record(unit_hex_line, 0x01);
    hex_line_set_checksum(unit_hex_line, 0xFF);

    hexError_t err;
    err = hex_check_checksum(unit_hex_line);
    ck_assert(HEX_ESUCCESS == err);
}
END_TEST

START_TEST (test_hex_check_checksum3)
{
    hex_line_set_record(unit_hex_line, 0x01);
    hex_line_set_checksum(unit_hex_line, 0xFA);

    hexError_t err;
    err = hex_check_checksum(unit_hex_line);
    ck_assert(HEX_ESUCCESS != err);
}
END_TEST

START_TEST (test_hex_check_checksum4)
{
    uint8_t *data = (uint8_t *) calloc(1, 4);
    ck_assert(NULL != data);
    data[0] = 0x12;
    data[1] = 0x34;
    data[2] = 0x56;
    data[3] = 0x78;
    hex_new_hex_line(&unit_hex_line, 0x04, 0xABCD, 0x00, data, 0x70);

    hexError_t err;
    err = hex_check_checksum(unit_hex_line);
    ck_assert(HEX_ESUCCESS == err);
}
END_TEST

START_TEST (test_hex_check_checksum5)
{
    uint8_t *data = (uint8_t *) calloc(1, 4);
    ck_assert(NULL != data);
    data[0] = 0x12;
    data[1] = 0x34;
    data[2] = 0x56;
    data[3] = 0x78;
    hex_new_hex_line(&unit_hex_line, 0x04, 0xABCD, 0x00, data, 0x91);

    hexError_t err;
    err = hex_check_checksum(unit_hex_line);
    ck_assert(HEX_ESUCCESS != err);
}
END_TEST

// Testing hex_check_record() function
START_TEST (test_hex_check_record1)
{
    hex_line_set_record(unit_hex_line, 0x02);

    hexError_t err;
    err = hex_check_record(unit_hex_line);
    ck_assert(HEX_ESUCCESS != err && HEX_ENONFATAL != err);
}
END_TEST

START_TEST (test_hex_check_record2)
{
    hex_line_set_record(unit_hex_line, 0x04);

    hexError_t err;
    err = hex_check_record(unit_hex_line);
    ck_assert(HEX_ESUCCESS != err && HEX_ENONFATAL != err);
}
END_TEST

START_TEST (test_hex_check_record3)
{
    hex_line_set_record(unit_hex_line, 0x00);

    hexError_t err;
    err = hex_check_record(unit_hex_line);
    ck_assert(HEX_ESUCCESS == err);
}
END_TEST

START_TEST (test_hex_check_record4)
{
    hex_line_set_record(unit_hex_line, 0x01);

    hexError_t err;
    err = hex_check_record(unit_hex_line);
    ck_assert(HEX_ESUCCESS == err);
}
END_TEST

START_TEST (test_hex_check_record5)
{
    hex_line_set_record(unit_hex_line, 0x05);

    hexError_t err;
    err = hex_check_record(unit_hex_line);
    ck_assert(HEX_ENONFATAL == err);
}
END_TEST

// Testing hex_parse_hex_line() function
START_TEST (test_hex_parse_hex_line1)
{   
    hex_new_hex_line(&unit_hex_line, 0x0, 0x0, 0x01, NULL, 0xFF);

    hexError_t err;
    hex_line *temp = NULL;
    err = hex_new_hex_line(&temp, 0x0, 0x0, 0x0, NULL, 0x0);
    ck_assert(HEX_ESUCCESS == err);

    const char *string = ":00000001FF";
    err = hex_parse_hex_line(temp, string);

    ck_assert(HEX_ESUCCESS == err);
    ck_assert(temp->count == unit_hex_line->count);
    ck_assert(temp->address == unit_hex_line->address);
    ck_assert(temp->data == unit_hex_line->data);
    ck_assert(NULL == temp->data);
    ck_assert(temp->record == unit_hex_line->record);
    ck_assert(temp->checksum == unit_hex_line->checksum);
    free_hex_line(temp);
}
END_TEST

START_TEST (test_hex_parse_hex_line2)
{
    uint8_t *data = (uint8_t *) calloc(1, 4);
    ck_assert(NULL != data);
    data[0] = 0x12;
    data[1] = 0x34;
    data[2] = 0x56;
    data[3] = 0x78;
    hex_new_hex_line(&unit_hex_line, 0x04, 0xABCD, 0x00, data, 0xF3);

    hexError_t err;
    hex_line *temp = NULL;
    err = hex_new_hex_line(&temp, 0x0, 0x0, 0x0, 0x0, 0x0);
    ck_assert(HEX_ESUCCESS == err);

    const char *string = ":04ABCD0012345678F3";
    err = hex_parse_hex_line(temp, string);

    ck_assert(HEX_ESUCCESS == err);
    ck_assert(temp->count == unit_hex_line->count);
    ck_assert(temp->address == unit_hex_line->address);
    ck_assert(0 == memcmp(temp->data, unit_hex_line->data, 4));
    ck_assert(temp->record == unit_hex_line->record);
    ck_assert(temp->checksum == unit_hex_line->checksum);
    free_hex_line(temp);
}
END_TEST

START_TEST (test_hex_parse_hex_line3)
{   
    hex_new_hex_line(&unit_hex_line, 0x0, 0x0, 0x01, NULL, 0xFF);

    hexError_t err;
    hex_line *temp = NULL;
    err = hex_new_hex_line(&temp, 0x0, 0x0, 0x0, NULL, 0x0);
    ck_assert(HEX_ESUCCESS == err);

    const char *string = ":00000001FF\n";
    err = hex_parse_hex_line(temp, string);

    ck_assert(HEX_ESUCCESS == err);
    ck_assert(temp->count == unit_hex_line->count);
    ck_assert(temp->address == unit_hex_line->address);
    ck_assert(temp->data == unit_hex_line->data);
    ck_assert(NULL == temp->data);
    ck_assert(temp->record == unit_hex_line->record);
    ck_assert(temp->checksum == unit_hex_line->checksum);
    free_hex_line(temp);
}
END_TEST

START_TEST (test_hex_parse_hex_line4)
{
    uint8_t *data = (uint8_t *) calloc(1, 4);
    ck_assert(NULL != data);
    data[0] = 0x12;
    data[1] = 0x34;
    data[2] = 0x56;
    data[3] = 0x78;
    hex_new_hex_line(&unit_hex_line, 0x04, 0xABCD, 0x00, data, 0xF3);

    hexError_t err;
    hex_line *temp = NULL;
    err = hex_new_hex_line(&temp, 0x0, 0x0, 0x0, 0x0, 0x0);
    ck_assert(HEX_ESUCCESS == err);

    const char *string = ":04ABCD0012345678F3\n";
    err = hex_parse_hex_line(temp, string);

    ck_assert(HEX_ESUCCESS == err);
    ck_assert(temp->count == unit_hex_line->count);
    ck_assert(temp->address == unit_hex_line->address);
    ck_assert(0 == memcmp(temp->data, unit_hex_line->data, 4));
    ck_assert(temp->record == unit_hex_line->record);
    ck_assert(temp->checksum == unit_hex_line->checksum);
    free_hex_line(temp);
}
END_TEST

Suite *test_hex_line_suite(void)
{
    Suite *s = suite_create("test hex line");

    TCase *tc_line = tcase_create("test hex line");
    tcase_add_checked_fixture(tc_line, unit_setup, unit_teardown);

    tcase_add_test(tc_line, test_hex_new_hex_line_null);
    tcase_add_test(tc_line, test_hex_new_hex_line1);
    tcase_add_test(tc_line, test_hex_line_set_count);
    tcase_add_test(tc_line, test_hex_line_set_address);
    tcase_add_test(tc_line, test_hex_line_set_record);
    tcase_add_test(tc_line, test_hex_line_set_data);
    tcase_add_test(tc_line, test_hex_line_set_checksum);

    tcase_add_test(tc_line, test_hex_check_checksum1);
    tcase_add_test(tc_line, test_hex_check_checksum2);
    tcase_add_test(tc_line, test_hex_check_checksum3);
    tcase_add_test(tc_line, test_hex_check_checksum4);
    tcase_add_test(tc_line, test_hex_check_checksum5);

    tcase_add_test(tc_line, test_hex_check_record1);
    tcase_add_test(tc_line, test_hex_check_record2);
    tcase_add_test(tc_line, test_hex_check_record3);
    tcase_add_test(tc_line, test_hex_check_record4);
    tcase_add_test(tc_line, test_hex_check_record5);
    tcase_add_test(tc_line, test_hex_parse_hex_line1);
    tcase_add_test(tc_line, test_hex_parse_hex_line2);
    tcase_add_test(tc_line, test_hex_parse_hex_line3);
    tcase_add_test(tc_line, test_hex_parse_hex_line4);
    suite_add_tcase(s, tc_line);
    return s;
}