#define _GNU_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <hex.h>
#include <unit_fname.h>

START_TEST (test_hex_create_new_filename_null1)
{   
    hexError_t err;
    err = hex_create_new_filename(NULL, "test");
    ck_assert(HEX_ESUCCESS != err);
}
END_TEST

START_TEST (test_hex_create_new_filename_null2)
{   
    char *original = "QaZyTesqA";
    char *output = original;

    hexError_t err;
    err = hex_create_new_filename(&output, NULL);
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(0 == strncmp(output, "QaZyTesqA", 9));
    ck_assert(output == original);
}
END_TEST

START_TEST (test_hex_create_new_filename_no_ext)
{   
    char *original = "QaZyTesqA";
    char *output = original;

    hexError_t err;
    err = hex_create_new_filename(&output, "test");
    ck_assert(HEX_ESUCCESS == err);
    ck_assert(0 == strcmp(output, "test.c"));
    ck_assert(output != original);
    free(output);
}
END_TEST

START_TEST (test_hex_create_new_filename_hex_ext)
{   
    char *original = "QaZyTesqA";
    char *output = original;

    hexError_t err;
    err = hex_create_new_filename(&output, "test.hex");
    ck_assert(HEX_ESUCCESS == err);
    ck_assert(0 == strcmp(output, "test.c"));
    ck_assert(output != original);
    free(output);
}
END_TEST

START_TEST (test_hex_create_new_filename_any_ext)
{   
    char *original = "QaZyTesqA";
    char *output = original;

    hexError_t err;
    err = hex_create_new_filename(&output, "test.bla-bla");
    ck_assert(HEX_ESUCCESS == err);
    ck_assert(0 == strcmp(output, "test.c"));
    ck_assert(output != original);
    free(output);
}
END_TEST

START_TEST (test_hex_create_new_filename_mult_ext1)
{   
    char *original = "QaZyTesqA";
    char *output = original;

    hexError_t err;
    err = hex_create_new_filename(&output, "test.ext1.ext2");
    ck_assert(HEX_ESUCCESS == err);
    ck_assert(0 == strcmp(output, "test.ext1.c"));
    ck_assert(output != original);
    free(output);
}
END_TEST

START_TEST (test_hex_create_new_filename_mult_ext2)
{   
    char *original = "QaZyTesqA";
    char *output = original;

    hexError_t err;
    err = hex_create_new_filename(&output, "test.ext1.hex");
    ck_assert(HEX_ESUCCESS == err);
    ck_assert(0 == strcmp(output, "test.ext1.c"));
    ck_assert(output != original);
    free(output);
}
END_TEST

START_TEST (test_hex_create_new_filename_mult_ext3)
{   
    char *original = "QaZyTesqA";
    char *output = original;

    hexError_t err;
    err = hex_create_new_filename(&output, "test.c.hex");
    ck_assert(HEX_ESUCCESS == err);
    ck_assert(0 == strcmp(output, "test.c.c"));
    ck_assert(output != original);
    free(output);
}
END_TEST

START_TEST (test_hex_create_new_filename_dir1)
{   
    char *original = "QaZyTesqA";
    char *output = original;

    hexError_t err;
    err = hex_create_new_filename(&output, "tests/test.hex");
    ck_assert(HEX_ESUCCESS == err);
    ck_assert(0 == strcmp(output, "test.c"));
    ck_assert(output != original);
    free(output);
}
END_TEST

START_TEST (test_hex_create_new_filename_dir2)
{   
    char *original = "QaZyTesqA";
    char *output = original;

    hexError_t err;
    err = hex_create_new_filename(&output, "./test1/test2/test.hex");
    ck_assert(HEX_ESUCCESS == err);
    ck_assert(0 == strcmp(output, "test.c"));
    ck_assert(output != original);
    free(output);
}
END_TEST

START_TEST (test_hex_create_new_filename_dir3)
{   
    char *original = "QaZyTesqA";
    char *output = original;

    hexError_t err;
    err = hex_create_new_filename(&output, "./test1/test2.hex/test.hex");
    ck_assert(HEX_ESUCCESS == err);
    ck_assert(0 == strcmp(output, "test.c"));
    ck_assert(output != original);
    free(output);
}
END_TEST

START_TEST (test_hex_create_new_filename_dir4)
{   
    char *original = "QaZyTesqA";
    char *output = original;

    hexError_t err;
    err = hex_create_new_filename(&output, "./test1/test2.hex/");
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(0 == strncmp(output, "QaZyTesqA", 9));
    ck_assert(output == original);
}
END_TEST

START_TEST (test_hex_create_new_filename_dir5)
{   
    char *original = "QaZyTesqA";
    char *output = original;

    hexError_t err;
    err = hex_create_new_filename(&output, "/");
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(0 == strncmp(output, "QaZyTesqA", 9));
    ck_assert(output == original);
}
END_TEST

START_TEST (test_hex_create_new_filename_dir6)
{   
    char *original = "QaZyTesqA";
    char *output = original;

    hexError_t err;
    err = hex_create_new_filename(&output, ".");
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(0 == strncmp(output, "QaZyTesqA", 9));
    ck_assert(output == original);
}
END_TEST

Suite *test_filename_suite(void)
{
    Suite *s = suite_create("filename creation");
    TCase *tc_fname = tcase_create("filename creation");
    tcase_add_test(tc_fname, test_hex_create_new_filename_null1);
    tcase_add_test(tc_fname, test_hex_create_new_filename_null2);
    tcase_add_test(tc_fname, test_hex_create_new_filename_no_ext);
    tcase_add_test(tc_fname, test_hex_create_new_filename_hex_ext);
    tcase_add_test(tc_fname, test_hex_create_new_filename_any_ext);
    tcase_add_test(tc_fname, test_hex_create_new_filename_mult_ext1);
    tcase_add_test(tc_fname, test_hex_create_new_filename_mult_ext2);
    tcase_add_test(tc_fname, test_hex_create_new_filename_mult_ext3);
    
    tcase_add_test(tc_fname, test_hex_create_new_filename_dir1);
    tcase_add_test(tc_fname, test_hex_create_new_filename_dir2);
    tcase_add_test(tc_fname, test_hex_create_new_filename_dir3);
    tcase_add_test(tc_fname, test_hex_create_new_filename_dir4);
    tcase_add_test(tc_fname, test_hex_create_new_filename_dir5);
    //tcase_add_test(tc_fname, test_hex_create_new_filename_dir6);

    suite_add_tcase(s, tc_fname);
    return s;
}