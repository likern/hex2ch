#define _GNU_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <hex.h>
#include <unit_args.h>

static struct
{
    // Path to the existing file, which doesn't have file extension
    char *g_path_to_exist_file_no_ext;

    // Path to the existing file, which has file extension
    char *g_path_to_exist_file_has_ext;
    int g_desc_to_file_no_ext;
    int g_desc_to_file_has_ext;

    // The same but path points to non-existing file
    char *g_path_to_non_exist_file_no_ext;
    char *g_path_to_non_exist_file_has_ext;

    // Path to the existing file with strict permissions
    char *g_path_to_exist_file_perm;
    int g_desc_to_exist_file_perm;

    // Path to the directory
    char *g_path_to_exist_directory;
} u_glb_conf;

static char *unit_argv[3];
static int unit_argc;

static void unit_emul_args_set(char *first, char *second, int argc)
{
    unit_argv[0] = "unit_hex";
    unit_argv[1] = first;
    unit_argv[2] = second;
    unit_argc = argc;
}

static void unit_emul_args_set_argc(int argc)
{
    unit_argc = argc;
}

static void unit_emul_args_set_argv_elem(char *value, int num)
{
    unit_argv[num] = value;
}

static char *unit_emul_args_get_argv_elem(int num)
{
    return unit_argv[num];
}

static char **unit_emul_args_get_argv_addr()
{
    return unit_argv;
}

static int unit_emul_args_get_argc()
{
    return unit_argc;
}

static void unit_setup()
{
    // Paths to non-exist files, just generate any
    u_glb_conf.g_path_to_non_exist_file_no_ext = "fQwErTytest1";
    u_glb_conf.g_path_to_non_exist_file_has_ext = "fAsDfGhtest2.hex";
    
    u_glb_conf.g_path_to_exist_file_no_ext = "fZxCvBntest3";
    int fd = open(u_glb_conf.g_path_to_exist_file_no_ext,
                   O_RDONLY | O_CREAT | O_EXCL, S_IRWXU);
    ck_assert(-1 != fd);
    u_glb_conf.g_desc_to_file_no_ext = fd;

    u_glb_conf.g_path_to_exist_file_has_ext = "fYuIoPtest4.any";
    fd = open(u_glb_conf.g_path_to_exist_file_has_ext,
                   O_RDONLY | O_CREAT | O_EXCL, S_IRWXU);
    ck_assert(-1 != fd);
    u_glb_conf.g_desc_to_file_has_ext = fd;

    // Create file without appropriate permissions
    u_glb_conf.g_path_to_exist_file_perm = "fSlIoWzQpO8";
    // No read rights
    mode_t msk = umask(S_IWUSR);
    fd = open(u_glb_conf.g_path_to_exist_file_perm,
                    O_WRONLY | O_CREAT | O_EXCL, S_IWUSR);
    ck_assert(-1 != fd);
    u_glb_conf.g_desc_to_exist_file_perm = fd;
    umask(msk);

    // Create directory
    u_glb_conf.g_path_to_exist_directory = "dNAs4QEFa";
    int res = mkdir(u_glb_conf.g_path_to_exist_directory, S_IRWXU);
    ck_assert(0 == res);
}

static void unit_teardown()
{
    int res = close(u_glb_conf.g_desc_to_file_no_ext);
    ck_assert(-1 != res);
    res = close(u_glb_conf.g_desc_to_file_has_ext);
    ck_assert(-1 != res);
    res = unlink(u_glb_conf.g_path_to_exist_file_no_ext);
    ck_assert(-1 != res);
    res = unlink(u_glb_conf.g_path_to_exist_file_has_ext);
    ck_assert(-1 != res);

    res = rmdir(u_glb_conf.g_path_to_exist_directory);
    ck_assert(0 == res);

    res = close(u_glb_conf.g_desc_to_exist_file_perm);
    ck_assert(-1 != res);
    res = unlink(u_glb_conf.g_path_to_exist_file_perm);
    ck_assert(-1 != res);
}

START_TEST (test_input_data_argv_null)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(NULL, "6454", 3);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 1
    uint32_t num = 0;
    hexError_t err;
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS != err);
}
END_TEST

START_TEST (test_input_data_argv_non_exist_file1)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_non_exist_file_no_ext, "6454", 3);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 2
    uint32_t num = 1234;
    hexError_t err;
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(1234 == num);
}
END_TEST

START_TEST (test_input_data_argv_non_exist_file2)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_non_exist_file_has_ext, "6454", 3);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 3
    uint32_t num = 1234;
    hexError_t err;
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(1234 == num);
}
END_TEST

START_TEST (test_input_data_argv_exist_file1)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_exist_file_has_ext, "6454", 3);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 4
    hexError_t err;
    err = hex_check_input_data_is_valid(NULL, argc, argv);
    ck_assert(HEX_ESUCCESS == err);
}
END_TEST

START_TEST (test_input_data_argv_exist_file2)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_exist_file_no_ext, "6454", 3);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 5
    uint32_t num = 0;
    hexError_t err;
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS == err);
    ck_assert(6454 == num);
}
END_TEST

START_TEST (test_input_data_argv_exist_file_perm)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_exist_file_perm, "6454", 3);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test
    uint32_t num = 1234;
    hexError_t err;
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(1234 == num);
}
END_TEST

START_TEST (test_input_data_argv_array_size_overflow)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_exist_file_no_ext, "65537", 3);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 6
    uint32_t num = 1593;
    hexError_t err;
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(1593 == num);
}
END_TEST

START_TEST (test_input_data_argv_array_size1)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_exist_file_no_ext, "65536", 3);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 7
    uint32_t num = 1593;
    hexError_t err;
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS == err);
    ck_assert(65536 == num);
}
END_TEST

START_TEST (test_input_data_argv_array_size2)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_exist_file_no_ext, "0", 3);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 8
    uint32_t num = 1593;
    hexError_t err;
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS == err);
    ck_assert(0 == num);
}
END_TEST

START_TEST (test_input_data_argv_array_underflow)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_exist_file_no_ext, "-1", 3);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 9
    uint32_t num = 1593;
    hexError_t err;
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(1593 == num);
}
END_TEST

START_TEST (test_input_data_argv_array_is_positive_hex)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_exist_file_no_ext, "0xFF", 3);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 10
    uint32_t num = 1593;
    hexError_t err;
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(1593 == num);
}
END_TEST

START_TEST (test_input_data_argv_array_is_negative_hex)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_exist_file_no_ext, "-0xFF", 3);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 11
    uint32_t num = 1593;
    hexError_t err;
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(1593 == num);
}
END_TEST

START_TEST (test_input_data_argv_array_is_positive_octal)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_exist_file_no_ext, "01234", 3);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 12
    uint32_t num = 1593;
    hexError_t err;
    // Should treat 0 prefix as decimal value: 01234 = 1234
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS == err);
    ck_assert(1234 == num);
}
END_TEST

START_TEST (test_input_data_argv_array_is_negative_octal)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_exist_file_no_ext, "-01234", 3);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 13
    uint32_t num = 1593;
    hexError_t err;
    // Should treat 0 prefix as decimal value: 01234 = 1234
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(1593 == num);
}
END_TEST

START_TEST (test_input_data_argv_help1)
{
    // Prepare argc and argv parameters
    unit_emul_args_set("-h", NULL, 2);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 14
    uint32_t num = 1593;
    hexError_t err;
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(1593 == num);
}
END_TEST

START_TEST (test_input_data_argv_help2)
{
    // Prepare argc and argv parameters
    unit_emul_args_set("--help", NULL, 2);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 15
    uint32_t num = 1593;
    hexError_t err;
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(1593 == num);
}
END_TEST

START_TEST (test_input_data_argv_help3)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(NULL, NULL, 1);
    int argc = unit_emul_args_get_argc();
    char **argv = unit_emul_args_get_argv_addr();

    // Test 16
    uint32_t num = 1593;
    hexError_t err;
    err = hex_check_input_data_is_valid(&num, argc, argv);
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(1593 == num);
}
END_TEST

START_TEST (test_hex_check_file_is_valid_is_null)
{
    hexError_t err;
    err = hex_check_file_is_valid(NULL, NULL);
    ck_assert(HEX_ESUCCESS != err);
}
END_TEST

START_TEST (test_hex_check_file_is_valid_not_exist)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_non_exist_file_no_ext, "6454", 3);
    char **argv = unit_emul_args_get_argv_addr();

    hexError_t err;
    err = hex_check_file_is_valid(argv[1], NULL);
    ck_assert(HEX_ESUCCESS != err);
}
END_TEST

START_TEST (test_hex_check_file_is_valid_not_regular)
{
    // Prepare argc and argv parameters
    unit_emul_args_set(u_glb_conf.g_path_to_exist_directory, "6454", 3);
    char **argv = unit_emul_args_get_argv_addr();

    hexError_t err;
    err = hex_check_file_is_valid(argv[1], NULL);
    ck_assert(HEX_ESUCCESS != err);
}
END_TEST

START_TEST (test_hex_check_number_is_null1)
{
    hexError_t err;
    err = hex_check_number_is_valid(NULL, NULL);
    ck_assert(HEX_ESUCCESS != err);
}
END_TEST

START_TEST (test_hex_check_number_is_null2)
{
    uint32_t number = 0;
    hexError_t err;
    err = hex_check_number_is_valid(&number, NULL);
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(0 == number);
}
END_TEST

START_TEST (test_hex_check_number_is_null3)
{
    hexError_t err;
    err = hex_check_number_is_valid(NULL, "512");
    ck_assert(HEX_ESUCCESS == err);
}
END_TEST

START_TEST (test_hex_check_number_is_float1)
{
    hexError_t err;
    err = hex_check_number_is_valid(NULL, "3.5");
    ck_assert(HEX_ESUCCESS != err);
}
END_TEST

START_TEST (test_hex_check_number_is_float2)
{
    hexError_t err;
    err = hex_check_number_is_valid(NULL, "-4.7");
    ck_assert(HEX_ESUCCESS != err);
}
END_TEST

START_TEST (test_hex_check_number_is_float3)
{
    hexError_t err;
    err = hex_check_number_is_valid(NULL, "0.0");
    ck_assert(HEX_ESUCCESS != err);
}
END_TEST

START_TEST (test_hex_check_number_is_float4)
{
    uint32_t number = 0;
    hexError_t err;
    // Should not modify number in failure case
    err = hex_check_number_is_valid(&number, "10.0");
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(0 == number);
}
END_TEST

START_TEST (test_hex_check_number_is_hex1)
{
    uint32_t number = 0;
    hexError_t err;
    err = hex_check_number_is_valid(&number, "0x512");
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(0 == number);
}
END_TEST

START_TEST (test_hex_check_number_is_hex2)
{
    uint32_t number = 0;
    hexError_t err;
    err = hex_check_number_is_valid(&number, "-0xFF");
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(0 == number);
}
END_TEST

START_TEST (test_hex_check_number_is_negative)
{
    uint32_t number = 1234;
    hexError_t err;
    err = hex_check_number_is_valid(&number, "-512");
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(1234 == number);
}
END_TEST

START_TEST (test_hex_check_number_is_overflow)
{
    int size = 512;
    char array[size];
    int res = snprintf(array, sizeof(uint64_t), "%" PRIu64, UINT64_MAX);
    array[res] = '\0';

    uint32_t number = 1234;
    hexError_t err;

    err = hex_check_number_is_valid(&number, array);
    ck_assert(HEX_ESUCCESS != err);
    ck_assert(1234 == number);
}
END_TEST

START_TEST (test_warn)
{
    // Should be warned that we didn't include this test to the tcase
}
END_TEST



Suite *test_input_data_suite(void)
{
    Suite *s = suite_create("checking command line arguments");
    
    // Input data checking
    TCase *tc_input = tcase_create("check input data");
    tcase_add_unchecked_fixture(tc_input, unit_setup, unit_teardown);
    tcase_add_test(tc_input, test_input_data_argv_null);
    tcase_add_test(tc_input, test_input_data_argv_non_exist_file1);
    tcase_add_test(tc_input, test_input_data_argv_non_exist_file2);
    tcase_add_test(tc_input, test_input_data_argv_exist_file1);
    tcase_add_test(tc_input, test_input_data_argv_exist_file2);
    tcase_add_test(tc_input, test_input_data_argv_exist_file_perm);
    
    tcase_add_test(tc_input, test_input_data_argv_array_size_overflow);
    tcase_add_test(tc_input, test_input_data_argv_array_underflow);
    tcase_add_test(tc_input, test_input_data_argv_array_size1);
    tcase_add_test(tc_input, test_input_data_argv_array_size2);
    tcase_add_test(tc_input, test_input_data_argv_array_is_positive_hex);
    tcase_add_test(tc_input, test_input_data_argv_array_is_negative_hex);
    tcase_add_test(tc_input, test_input_data_argv_array_is_positive_octal);
    tcase_add_test(tc_input, test_input_data_argv_array_is_negative_octal);
    
    // Should print help message if run without arguments,
    // or with "--help" or "-h"
    tcase_add_test(tc_input, test_input_data_argv_help1);
    tcase_add_test(tc_input, test_input_data_argv_help2);
    tcase_add_test(tc_input, test_input_data_argv_help3);

    tcase_add_test(tc_input, test_hex_check_file_is_valid_is_null);
    tcase_add_test(tc_input, test_hex_check_file_is_valid_not_exist);
    tcase_add_test(tc_input, test_hex_check_file_is_valid_not_regular);

    tcase_add_test(tc_input, test_hex_check_number_is_null1);
    tcase_add_test(tc_input, test_hex_check_number_is_null2);
    tcase_add_test(tc_input, test_hex_check_number_is_null3);
    tcase_add_test(tc_input, test_hex_check_number_is_float1);
    tcase_add_test(tc_input, test_hex_check_number_is_float2);
    tcase_add_test(tc_input, test_hex_check_number_is_float3);
    tcase_add_test(tc_input, test_hex_check_number_is_float4);
    tcase_add_test(tc_input, test_hex_check_number_is_hex1);
    tcase_add_test(tc_input, test_hex_check_number_is_hex2);
    tcase_add_test(tc_input, test_hex_check_number_is_negative);
    tcase_add_test(tc_input, test_hex_check_number_is_overflow);

    suite_add_tcase(s, tc_input);
    return s;
}