#ifndef __UNIT_ARGS_H__
#define __UNIT_ARGS_H__

#include <check.h>

// Returns suite with all tests responsible to
// input data (command line arguments) testing
Suite *test_input_data_suite(void);

#endif
