#ifndef __HEX_H__
#define __HEX_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <hex_base.h>
#include <hex_error.h>
#include <hex_check.h>
#include <hex_print.h>
#include <hex_string.h>
#include <hex_io.h>
#include <hex_mem.h>

hexError_t
hex_new_hex_line(hex_line **line, uint8_t count, uint16_t address,
                 uint8_t record, uint8_t *data, uint8_t checksum);

hexError_t
hex_new_hex_line_empty(hex_line **line);

hexError_t
hex_line_set_count(hex_line *line, uint8_t count);

hexError_t
hex_line_set_address(hex_line *line, uint16_t address);

hexError_t
hex_line_set_record(hex_line *line, uint8_t record);

hexError_t
hex_line_set_data(hex_line *line, uint8_t *data);

hexError_t
hex_line_set_checksum(hex_line *line, uint8_t checksum);

// Free memory allocated by hex_line structure
void free_hex_line(hex_line *hexl);

// Free continuous array of hex_line structures
void free_hex_line_array(hex_line **hexl, size_t elem);

hexError_t
hex_get_basename(char **name, const char *original);

// Create new filename substituting file extension to .c
hexError_t
hex_create_new_filename(char **newfname, const char *original);

// Comparison function, used to reorder hex_line array
int hex_cmp_sort(const void *first, const void *second);

// Sorting function, which sorts with compare predicate
hexError_t
hex_sort_hexline(void *base, size_t elem, size_t size,
                 int (*compare) (const void *, const void *));


#endif
