#ifndef __HEX_CHECK_H__
#define __HEX_CHECK_H__

#include <stdbool.h>
#include <hex_base.h>
#include <hex_error.h>

#define HEX_CHECK_MIN_NUMBER 0
#define HEX_CHECK_MAX_NUMBER 65536

// Check Intel HEX line contains valid checksum
hexError_t
hex_check_checksum(hex_line *hexl);

// Check Intel HEX line contains valid record type
// If return valus is HEX_ENONFATAL should skip over
// this line (ignore) and continue working
hexError_t
hex_check_record(hex_line *hexl);

hexError_t
hex_check_is_last_byte(bool *result, size_t current, size_t max);

hexError_t
hex_check_record_is_eof(bool *val, hex_line *hexl);

// Parse line and fill in hex_line structure.
// This function doesn't perform any checks, only 
// that line has appropriate bytes for every field.
hexError_t
hex_parse_hex_line(hex_line *hexl, const char *line);

// Check that string encoded number is
// valid positive decimal integer
hexError_t
hex_check_number_is_valid(uint32_t *number, const char *string);

// Check that file exists, regular and has read rights
hexError_t 
hex_check_file_is_valid(const char *path, hexFileStat_t *finfo);

// Check that user has read rights on this file
hexError_t
hex_check_file_read_rights(hexFileStat_t *info);

// Check input argc and argv supplied
// arguments are valid
hexError_t
hex_check_input_data_is_valid(uint32_t *num, int argc, char **argv);

#endif