#ifndef __HEX_BASE_H__
#define __HEX_BASE_H__

typedef struct _hex_line
{
    uint8_t count;
    uint16_t address;
    uint8_t record;
    uint8_t *data;
    uint8_t checksum;
} hex_line;

enum 
{      HEX_REC_DATA     = 0x00,
       HEX_REC_EOF      = 0x01,
       HEX_REC_ESEGADDR = 0x02,
       HEX_REC_SSEGADDR = 0x03,
       HEX_REC_ELINADDR = 0x04,
       HEX_REC_SLINADDR = 0x05
};

typedef struct stat hexFileStat_t;

#endif
