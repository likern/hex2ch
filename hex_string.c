#define _GNU_SOURCE

#include <string.h>
#include <ctype.h>

#include <hex.h>

hexError_t
hex_str_to_upper(char **string, const char *filename)
{
    if (!string || !filename)
    {
        return HEX_EINVARG;
    }

    char *tmp_string = strdup(filename);
    if (!tmp_string)
    {
        return HEX_EALLOCATION;
    }

    char *iter = tmp_string;
    while (*iter != '\0')
    {
        if (!isprint(*iter))
        {
            free(tmp_string);
            return HEX_ESTRFMT;
        }

        if (!isalnum(*iter))
        {
            *iter = '_';
        }
        else
        {
            *iter = toupper(*iter);
        }
        ++iter;
    }
    *string = tmp_string;
    return HEX_ESUCCESS;
}