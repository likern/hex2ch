//#include <errno.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <unistd.h>
//#include <string.h>
//#include <inttypes.h>

#include <hex.h>

hexError_t
hex_malloc(void **buffer, size_t size)
{
    if (!size)
    {
        // Allocating 0 bytes does not make sense.
        // But malloc() could return 0 or pointer
        // which can't be dereferenced. It's
        // implementation-defined.
        return HEX_EALLOCATION;
    }

    void *memory = malloc(size);
    if (!memory)
    {
        return HEX_EALLOCATION;
    }

    if (buffer)
    {
        *buffer = memory;
    }
    else
    {
        // It's allowed to pass NULL pointer to check
        // that allocating this amount of bytes is
        // possible.
        free(memory);
    }
    return HEX_ESUCCESS;
}

void
hex_free(void *ptr)
{
    // Passing NULL is allowed
    free(ptr);
}