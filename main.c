#include <unistd.h>
#include <hex.h>

int main(int argc, char **argv) 
{
    uint32_t number = 0;
    hexError_t err;
    err = hex_check_input_data_is_valid(&number, argc, argv);
    if (HEX_ESUCCESS != err)
    {
        HEX_DEBUG("Command line arguments are invalid.");
        exit(err);
    }

    char *sfilename;
    err = hex_create_new_filename(&sfilename, argv[1]);
    if (HEX_ESUCCESS != err)
    {
        HEX_DEBUG("Can't create new file name based on the given.");
        exit(err);
    }

    hexFile_t *finput = NULL;
    err = hex_open_file(&finput, argv[1], "r");
    if (HEX_ESUCCESS != err)
    {
        HEX_DEBUG("Can't open file [%s] for reading.", argv[1]);
        free(sfilename);
        exit(err);
    }

    hexFile_t *foutput = NULL;
    err = hex_open_file(&foutput, sfilename, "wx");
    if (HEX_ESUCCESS != err)
    {
        HEX_DEBUG("Can't create new file [%s].", sfilename);
        free(sfilename);
        hex_close_file(finput);
        exit(err);
    }

    err = hex_write_start_header_to_file(sfilename, foutput);
    if (HEX_ESUCCESS != err)
    {
        HEX_DEBUG("Can't write start header guard.");
        free(sfilename);
        hex_close_file(finput);
        hex_delete_file(foutput);
        exit(err);
    }

    size_t elem = 0;
    hex_line **hexl_array = NULL;
    // Load all Intel HEX file into memory
    err = hex_read_file_content(&hexl_array, &elem, finput);
    if (HEX_ESUCCESS != err)
    {
        free(sfilename);
        hex_close_file(finput);
        hex_delete_file(foutput);
        exit(err);
    }

    err = hex_sort_hexline(hexl_array, elem, sizeof(hex_line *), \
                           hex_cmp_sort);
    if (HEX_ESUCCESS != err)
    {
        HEX_DEBUG("Can't sort hex lines.");
        free_hex_line_array(hexl_array, elem);
        free(sfilename);
        hex_close_file(finput);
        hex_delete_file(foutput);
        exit(err);
    }

#ifdef H_DEBUG_MODE
    for (int k = 0; k < elem; ++k)
    {
        hex_line *hexl = hexl_array[k];
        fprintf(stderr, "line [%i]:\n", k);
        print_hex_line_to_err(hexl);
    }
#endif    

    // Write array to the header file
    fprintf(foutput->fd, "static const unsigned char hex_bytes");
    fprintf(foutput->fd, "[%s] = \n{\n", argv[2]);
    err = hex_write_array_to_file(hexl_array, elem, number, foutput);
    if (HEX_ESUCCESS != err)
    {
        free_hex_line_array(hexl_array, elem);
        free(sfilename);
        hex_close_file(finput);
        hex_delete_file(foutput);
        exit(err);
    }

    fprintf(foutput->fd, "\n};");
    err = hex_write_end_header_to_file(foutput);
    if (HEX_ESUCCESS != err)
    {
        HEX_DEBUG("Can't write end header guard.");
        free_hex_line_array(hexl_array, elem);
        free(sfilename);
        hex_close_file(finput);
        hex_delete_file(foutput);
        exit(err);
    }

    // Clear unneeded resources
    free_hex_line_array(hexl_array, elem);
    free(sfilename);
    hex_close_file(finput);
    hex_close_file(foutput);
    exit(HEX_ESUCCESS);
}