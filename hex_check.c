#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <inttypes.h>

#include <hex.h>

hexError_t
hex_check_number_is_valid(uint32_t *number, const char *string)
{
    if (!string)
    {
        return HEX_EINVARG;
    }

    errno = 0;
    int rest = 0;
    uint32_t temp = 0;
    int res = sscanf(string, "%"SCNu32"%n", &temp, &rest);
    if (0 != errno || 1 != res || string[rest] != '\0')
    {
        HEX_DEBUG("Integer [%s] is not valid decimal value", string);
        return HEX_ECONVERSION;
    }

    if (HEX_CHECK_MIN_NUMBER > temp ||
        HEX_CHECK_MAX_NUMBER < temp)
    {
        HEX_DEBUG("Integer [%s] is out of [%d, %d] bounds",
                  string, HEX_CHECK_MIN_NUMBER, HEX_CHECK_MAX_NUMBER);
        return HEX_ECONVERSION;
    }

    if (number)
    {
        *number = temp;
    }
    return HEX_ESUCCESS;
}

hexError_t 
hex_check_file_is_valid(const char *path, hexFileStat_t *finfo)
{
    struct stat info;
    if (!path)
    {
        HEX_DEBUG("argument is null pointer");
        return HEX_EINVARG;
    }
    else if (stat(path, &info))
    {
        HEX_DEBUG("system call stat() failed on %s", path);
        return HEX_ESYSTEM;
    }
    else if (!S_ISREG(info.st_mode))
    {
        HEX_DEBUG("file is not regular: %s", path);
        return HEX_EINVARG;
    }

    hexError_t err;
    err = hex_check_file_read_rights(&info);
    if (HEX_ESUCCESS != err)
    {
        HEX_DEBUG("no read permissions: %s", path);
        return err;
    }

    if (finfo)
    {
        *finfo = info;
    }
    return HEX_ESUCCESS;
}

hexError_t
hex_check_checksum(hex_line *hexl)
{
    if (!hexl)
    {
        return HEX_EINVARG;
    }

    uintmax_t sum;
    sum = hexl->count;

    if (hexl->address)
    {
        sum += *((uint8_t *) &hexl->address);
        sum += *(((uint8_t *) &hexl->address) + 1);
    }
    sum += hexl->record;

    for (int i = 0; i < hexl->count; ++i)
    {
        sum += hexl->data[i];
    }

    sum = (~sum + 1) & 0xFF;

    if (sum != hexl->checksum)
    {
        HEX_DEBUG("Valid checksum is: [0x%jX]", sum);
        return HEX_ECHECKSUM;
    }

    return HEX_ESUCCESS;
}

hexError_t
hex_check_record(hex_line *hexl)
{
    if (!hexl)
    {
        return HEX_EINVARG;
    }

    // Doesn't support Extended Segment Address Record
    // and Extended Linear Address Record
    if (HEX_REC_ESEGADDR == hexl->record ||
        HEX_REC_ELINADDR == hexl->record)
    {
        return HEX_ERECORD;
    }
    else if (HEX_REC_DATA != hexl->record &&
             HEX_REC_EOF != hexl->record)
    {
        return HEX_ENONFATAL;
    }
    return HEX_ESUCCESS;
}

hexError_t
hex_check_is_last_byte(bool *result, size_t current, size_t max)
{
    if (!result) 
    {
        return HEX_EINVARG;
    }

    if (current != max)
    {
        *result = false;
    }
    *result = true;
    return HEX_ESUCCESS;
}

hexError_t
hex_check_record_is_eof(bool *val, hex_line *hexl)
{
    if (!hexl)
    {
        return HEX_EINVARG;
    }

    if (HEX_REC_EOF != hexl->record)
    {
        if (val)
        {
            *val = false;
        }
        return HEX_ESUCCESS;
    }

    // The byte count should be 0x00 for EOF
    if (0x00 != hexl->count)
    {
        if (val)
        {
            *val = false;
        }
        return HEX_ERECORD;
    }

    // The data field should be also empty for EOF
    if (NULL != hexl->data)
    {
        if (val)
        {
            *val = false;
        }
        return HEX_ERECORD;
    }

    if (val)
    {
        *val = true;
    }
    return HEX_ESUCCESS;
}

hexError_t
hex_parse_hex_line(hex_line *hexl, const char *line)
{
    if (!line)
    {
        return HEX_EINVARG;
    }

    hex_line htemp;
    errno = 0;
    // parse start code, byte count, address, record type
    int rest = 0;
    int result = sscanf(line, ":%2"SCNx8"%4"SCNx16"%2"SCNx8"%n", 
                        &htemp.count, 
                        &htemp.address,
                        &htemp.record,
                        &rest);

    if (0 != errno || 3 != result)
    {
        return HEX_ECONVERSION;
    }

    if (!htemp.count)
    {
        htemp.data = 0x0;
    }
    else
    {
        htemp.data = calloc(1, htemp.count);
        if (!htemp.data)
        {
            return HEX_EALLOCATION;
        }
    }

    // parse data field
    for (int i = 0; i < htemp.count; ++i)
    {
        errno = 0;
        int res = 0;
        result = sscanf(line + rest, "%2"SCNx8"%n",
                        (htemp.data + i),
                        &res);
        rest += res;
        if (0 != errno || 1 != result)
        {
            free(htemp.data);
            return HEX_ECONVERSION;
        }
    }

    errno = 0;
    int res = 0;
    result = sscanf(line + rest, "%2"SCNx8"%n",
                    &htemp.checksum, &res);
    rest += res;
    if (0 != errno || 1 != result || 
        (*(line + rest)  != '\0' && *(line + rest)  != '\n') )
    {
        free(htemp.data);
        return HEX_ECONVERSION;
    }

    // Fill in structure only by demand,
    // when appropriate pointer is provided
    if (hexl)
    {
        *hexl = htemp;
    }
    else
    {
        free(htemp.data);
    }
    return HEX_ESUCCESS;
}

hexError_t
hex_check_file_read_rights(hexFileStat_t *info)
{
    if (!info)
    {
        return HEX_EINVARG;
    }

    uid_t user_id = getuid();
    gid_t group_id = getgid();

    int user_perm = info->st_mode & S_IRUSR;
    int group_perm = info->st_mode & S_IRGRP;
    int other_perm = info->st_mode & S_IROTH;

    if ((user_id == info->st_uid) && (S_IRUSR == user_perm))
    {
        // Owner and has read permission
        return HEX_ESUCCESS;
    }
    else if ((group_id == info->st_gid) && (S_IRGRP == group_perm))
    {
        // Belongs to the group and has read permission
        return HEX_ESUCCESS;
    }
    else
    {
        // Check supplementary groups
        int ngroups = getgroups(0, NULL);
        gid_t *memory = NULL;
        hexError_t err;
        err = hex_malloc((void **) &memory, ngroups * sizeof(gid_t));
        if (HEX_ESUCCESS != err)
        {
            return err;
        }

        int res = getgroups(ngroups, memory);
        if (res < 0)
        {
            hex_free(memory);
            return HEX_ESYSTEM;
        }

        for (int i = 0; i < ngroups; ++i)
        {
            if ((memory[i] == info->st_gid) && (S_IRGRP == group_perm))
            {
                hex_free(memory);
                return HEX_ESUCCESS;
            }
        }
        hex_free(memory);
    }

    if (other_perm)
    {
        return HEX_ESUCCESS;
    }
    return HEX_EPERM;
}

hexError_t
hex_check_input_data_is_valid(uint32_t *num, int argc, char **argv) {
    if (!argc || !argv)
    {
        print_help("intel-hex");
        return HEX_EINVARG;
    }
    else if (1 == argc)
    {
        print_help("intel-hex");
        return HEX_EINVARG;
    }
    else if (2 == argc)
    {
        if (argv && argv[1])
        {
            if (!strcmp("--help", argv[1]) || !strcmp("-h", argv[1]))
            {
                print_help("intel-hex");
                return HEX_EINVARG;
            }
            HEX_DEBUG("Invalid arguments.");
            print_simple_help("intel-hex");
            return HEX_EINVARG;
        }
        HEX_DEBUG("Something strange happens.");
        print_simple_help("intel-hex");
        return HEX_EINVARG;
    }

    if (3 != argc)
    {
        HEX_DEBUG("You should provide two arguments.");
        print_simple_help("intel-hex");
        return HEX_EINVARG;
    }

    hexError_t err;
    err = hex_check_file_is_valid(argv[1], NULL);
    if (HEX_ESUCCESS != err)
    {
        HEX_DEBUG("File [%s] does't exist or is not regular.", argv[1]);
        return HEX_EINVARG;
    }

    err = hex_check_number_is_valid(num, argv[2]);
    if (HEX_ESUCCESS != err)
    {
        return HEX_EINVARG;
    }
    return HEX_ESUCCESS;
}