#define _GNU_SOURCE

#include <unistd.h>
#include <string.h>
#include <inttypes.h>
#include <stdbool.h>
#include <errno.h>

#include <hex.h>

void free_hex_file(hexFile_t *file);

hexError_t
hex_open_file(hexFile_t **file, const char *path, const char *mode)
{
    if (!path || !mode)
    {
        return HEX_EINVARG;
    }

    FILE *fd_temp = fopen(path, mode);
    if (!fd_temp) 
    {
        HEX_DEBUG("fopen(): failed");
        return HEX_EIO;
    }

    if (file)
    {
        size_t len = strlen(path) + 1;
        char *npath = (char *) calloc(1, len);
        if (!npath)
        {
            fclose(fd_temp);
            return HEX_EALLOCATION;
        }

        hexFile_t *nfile = (hexFile_t *) calloc(1, sizeof(hexFile_t));
        if (!nfile)
        {
            free(npath);
            fclose(fd_temp);
            return HEX_EALLOCATION;
        }

        strcpy(npath, path);
        nfile->fd = fd_temp;
        nfile->path = npath;
        *file = nfile;
    }
    return HEX_ESUCCESS;
}

hexError_t
hex_close_file(hexFile_t *file)
{
    if (!file)
    {
        return HEX_EINVARG;
    }

    if (file->fd)
    {
        if (fclose(file->fd))
        {
            return HEX_EIO;
        }
        file->fd = NULL;
    }

    return HEX_ESUCCESS;
}

hexError_t
hex_delete_file(hexFile_t *file)
{
    if (!file)
    {
        return HEX_EINVARG;
    }

    hexError_t err;
    err = hex_close_file(file);
    if (HEX_ESUCCESS != err)
    {
        return err;
    }

    if (unlink(file->path))
    {
        return HEX_EIO;
    }

    free_hex_file(file);
    file = NULL;
    return HEX_ESUCCESS;
}

void free_hex_file(hexFile_t *file)
{
    if (file)
    {
        if (file->path)
        {
            free((void *) file->path);
        }
        free((void *) file);
    }
}

hexError_t
hex_read_file_content(hex_line ***hexl, size_t *elem, hexFile_t *file)
{
    if (!hexl || !elem || !file)
    {
        return HEX_EINVARG;
    }

    size_t size = HEX_SYMB_IN_LINE;
    char *buffer = (char *) malloc(size);
    if (!buffer)
    {
        return HEX_EALLOCATION;
    }

    size_t nlines = HEX_NUM_LINES;
    size_t ptr_size = sizeof(hex_line *);
    hex_line **harray = (hex_line **) calloc(nlines, ptr_size);
    if (!harray)
    {
        free(buffer);
        return HEX_EALLOCATION;
    }

    hex_line *tmp_hexl = NULL;
    uint32_t count = 0;
    hexError_t err;

    bool hex_eof = false;
    errno = 0;
    while (-1 != getline(&buffer, &size, file->fd))
    {
        err = hex_new_hex_line_empty(&tmp_hexl);
        if (HEX_ESUCCESS != err)
        {
            free(buffer);
            free_hex_line_array(harray, nlines);
            return err;
        }

        if (true == hex_eof)
        {
            // Should never get this. EOF didn't appear
            // in the last line
            HEX_DEBUG("Found EOF record not on the last line.");
            HEX_DEBUG("Next line: %s", buffer);
            free(buffer);
            free_hex_line_array(harray, nlines);
            free_hex_line(tmp_hexl);
            return HEX_ERECORD;
        }

        err = hex_parse_hex_line(tmp_hexl, buffer);
        if (HEX_ESUCCESS != err)
        {
            HEX_DEBUG("Found error in parsing line:");
            HEX_DEBUG("%s", buffer);
            free(buffer);
            free_hex_line_array(harray, nlines);
            free_hex_line(tmp_hexl);
            return err;
        }

        // Perform various checks before load
        // to the global memory;
        err = hex_check_checksum(tmp_hexl);
        if (HEX_ESUCCESS != err)
        {
            HEX_DEBUG("Found invalid checksum [0x%"
                      PRIX8"] on the line:", tmp_hexl->checksum);
            HEX_DEBUG("%s", buffer);
            free(buffer);
            free_hex_line_array(harray, nlines);
            free_hex_line(tmp_hexl);
            return err;
        }

        // Here if we get HEX_ENONFATAL
        // just ignore element and continue working
        err = hex_check_record(tmp_hexl);
        if (HEX_ENONFATAL == err)
        {
            // Free only allocated data field.
            // Others fields will be successfully
            // overwritten by the next hex_parse_hex_line() call
            HEX_DEBUG("Ignore line with the record type: [0x%"
                      PRIX8"]:", tmp_hexl->record);
            HEX_DEBUG("%s", buffer);
            free_hex_line(tmp_hexl);
            tmp_hexl = NULL;
            continue;
        }
        else if (HEX_ESUCCESS != err)
        {
            HEX_DEBUG("Found non-permissible record type: [0x%"
                      PRIX8"] in line:", tmp_hexl->record);
            HEX_DEBUG("%s", buffer);
            free(buffer);
            free_hex_line_array(harray, nlines);
            free_hex_line(tmp_hexl);
            return err;
        }

        err = hex_check_record_is_eof(&hex_eof, tmp_hexl);
        if (HEX_ESUCCESS != err)
        {
            free(buffer);
            free_hex_line_array(harray, nlines);
            free_hex_line(tmp_hexl);
            return err;
        }

        // Check that we have enough allocated memory
        if (count > HEX_NUM_LINES)
        {
            // We have to reallocate memory
            size_t new_nlines = (nlines + HEX_NUM_LINES) * ptr_size;
            hex_line **new_harray = (hex_line **) realloc(harray, new_nlines);
            if (!new_harray)
            {
                free(buffer);
                free_hex_line_array(harray, nlines);
                free_hex_line(tmp_hexl);
                return HEX_EIO;
            }

            harray = new_harray;
            nlines = new_nlines;
        }

        if (!hex_eof)
        {
            harray[count] = tmp_hexl;
            ++count;
        }
    }

    bool fail = false;
    if (0 != errno)
    {
        // Detectected error of getline() function
        HEX_DEBUG("getline() function failed");
        fail = true;
    }
    else if (!hex_eof)
    {
        HEX_DEBUG("Not found EOF line at the end of file");
        fail = true;
    }

    if (fail)
    {
        free(buffer);
        free_hex_line_array(harray, nlines);
        free_hex_line(tmp_hexl);
        return HEX_EIO;
    }
    free(buffer);

    *elem = count;
    *hexl = harray;
    return HEX_ESUCCESS;
}

hexError_t
hex_calculate_delimeter(const char **out, size_t current, size_t max)
{   const char *end_line = ",\n";
    const char *end_elem = ",";
    const char *end_all = "";

    if (!out)
    {
        return HEX_EINVARG;
    }

    const char *delim;
    bool last_elem_in_col = false;
    bool final_elem = false;

    if (current == (max - 1))
    {
        final_elem = true;
    }

    if (0 == ((current + 1) % HEX_PRINT_WIDTH))
    {
        last_elem_in_col = true;
    }

    if (final_elem)
    {
        delim = end_all;
    }
    else if (last_elem_in_col)
    {
        delim = end_line;
    }
    else
    {
        delim = end_elem;
    }
    *out = delim;
    return HEX_ESUCCESS;
}

hexError_t
hex_fmt_write_hex_line_data(hex_line *hexl, hexFile_t *file,
                        size_t start_pos, size_t max)
{
    if (!hexl || !file)
    {
        return HEX_EINVARG;
    }

    // FIXME: Improve speed, write to the files
    // by bigger data chunks

    hexError_t err;
    for (uint8_t i = 0; i < hexl->count; ++i)
    {
        const char *curr_delim;
        size_t current = start_pos + i;
        hex_calculate_delimeter(&curr_delim, current, max);
        err = hex_write_hex_byte_to_file(hexl->data[i], file, curr_delim);
        
        if (HEX_ESUCCESS != err)
        {
            return HEX_EIO;
        }
    }
    return HEX_ESUCCESS;
}

hexError_t
hex_fmt_write_hex_byte_to_file(uint8_t hex, hexFile_t *file,
                               size_t start_pos, size_t max)
{
    if (!file)
    {
        return HEX_EINVARG;
    }

    const char *curr_delim;
    hex_calculate_delimeter(&curr_delim, start_pos, max);
    hexError_t err;
    err = hex_write_hex_byte_to_file(hex, file, curr_delim);
    if (HEX_ESUCCESS != err)
    {
        return err;
    }
    return HEX_ESUCCESS;
}

hexError_t
hex_write_hex_byte_to_file(uint8_t hex, hexFile_t *file, const char *delim)
{
    if (!file)
    {
        return HEX_EINVARG;
    }

    const char *tmp_delim = "";
    if (delim)
    {
        tmp_delim = delim;
    }

    int res;
    res = fprintf(file->fd, "0x%" PRIX8 "%s", hex, tmp_delim);
    if (res < 0)
    {
        return HEX_EIO;
    }
    return HEX_ESUCCESS;
}

// Should supply array sorted by address field
hexError_t
hex_write_array_to_file(hex_line **hexl, size_t elem, size_t max, hexFile_t *file)
{
    if (!hexl || !file)
    {
        return HEX_EINVARG;
    }

    if (max < elem)
    {
        // If (max < elem) means some hexl[elem] will
        // outside of valid max range
        return HEX_EINVARG;
    }

    hexError_t err;
    // Start writing from the current offset
    for (int k = 0; k < max; )
    {
        static int j = 0;
        bool check = false;

        if (j < elem)
        {
            if (j < (elem - 1) && !check)
            {
                // Address overlapping detect
                if (hexl[j]->address + hexl[j]->count > hexl[j+1]->address)
                {
                    HEX_DEBUG("Addresses [0x%" PRIX16 "] and [0x%"PRIX16"]"
                              " are overlapping", hexl[j]->address,
                              hexl[j+1]->address);
                    return HEX_EOVERLAP;
                }
            }

            if ((hexl[j]->address + hexl[j]->count) > max && !check)
            {
                HEX_DEBUG("Address range [address]:[address+count]:");
                HEX_DEBUG("[0x%"PRIX16"]:[0x%"PRIX16"] exceeds valid range:",
                          hexl[j]->address, hexl[j]->address + hexl[j]->count);
                HEX_DEBUG("[0x%"PRIX16"]:[0x%"PRIX16"]", 0x0, (uint16_t) max);
                return HEX_EOUTOFBOUND;
            }
            check = true;

            if (k == hexl[j]->address)
            {
                err = hex_fmt_write_hex_line_data(hexl[j], file, k, max);
                if (HEX_ESUCCESS != err)
                {
                    return err;
                }

                k += hexl[j]->count;
                ++j;
                check = false;
            }
            else if (k < hexl[j]->address)
            {
                hex_fmt_write_hex_byte_to_file(0xFF, file, k, max);
                ++k;
            }
            else
            {
                HEX_DEBUG("Address [0x%"PRIX16"] is out of [%zu]",
                          hexl[k]->address, max);
                return err;
            }
        }
        else
        {
            hex_fmt_write_hex_byte_to_file(0xFF, file, k, max);
            ++k;
        }
    }
    return HEX_ESUCCESS;
}

hexError_t
hex_write_end_header_to_file(hexFile_t *file)
{
    if (!file)
    {
        return HEX_EINVARG;
    }

    errno = 0;
    int res = fseek(file->fd, 0, SEEK_END);
    if (res < 0)
    {
        return HEX_EIO;
    }
    res = fprintf(file->fd, "\n#endif");
    if (res < 0)
    {
        return HEX_EIO;
    }
    return HEX_ESUCCESS;
}

hexError_t
hex_write_start_header_to_file(const char *filename, hexFile_t *file)
{
    if (!filename || !file)
    {
        return HEX_EINVARG;
    }

    char *basename;
    hexError_t err;
    err = hex_get_basename(&basename, filename);
    if (HEX_ESUCCESS != err)
    {
        return err;
    }

    char *str_upper;
    err = hex_str_to_upper(&str_upper, basename);
    if (HEX_ESUCCESS != err)
    {
        return err;
    }

    errno = 0;
    int res = fprintf(file->fd, "#ifndef __%s_\n", str_upper);
    if (res < 0)
    {
        free(str_upper);
        return HEX_EIO;
    }

    free(str_upper);
    basename = NULL;
    str_upper = NULL;

    fpos64_t position;
    res = fgetpos64(file->fd, &position);
    if (res < 0)
    {
        return HEX_EIO;
    }

    fprintf(file->fd, "#endif");
    res = fsetpos64(file->fd, &position);
    if (res < 0)
    {
        return HEX_EIO;
    }
    return HEX_ESUCCESS;
}